![FastAPI](https://img.shields.io/badge/FastAPI-005571?logo=fastapi)
[![Postgres](https://img.shields.io/badge/postgres-%23316192.svg?style=for-the-badge&logo=postgresql&logoColor=white)](https://www.postgresql.org/)


[![Python](https://img.shields.io/badge/-Python-464646?style=flat-square&logo=Python)](https://www.python.org/)
[![docker](https://img.shields.io/badge/-Docker-464646?style=flat-square&logo=docker)](https://www.docker.com/)



# Salary Digests
## Описание

**«Salary Digests»** - это микросервис, где пользователи могут _получить_ информацию о своей зарплате. Для того чтобы 
получить информацию о зарплате клиент должен авторизироваться и сделать запрос на определенный url для получения информации.
Если клиена не содержиться в базе данных будет выведена информация о том что данный пользователь не обнаружен



# Функционал

- В проекте реализована аутенификация через jwt токен а так же подключение к базе данных postgres

# Технологии

- [Python 3.12](https://www.python.org/downloads/release/python-312/)
- [FastAPI](https://fastapi.tiangolo.com/)
- [PostgresSQL 13.0](https://www.postgresql.org/download/)


# Контейнер

- [Docker 20.10.14](https://www.docker.com/)
- [Docker Compose 2.4.1](https://docs.docker.com/compose/)

# URL's

- http://127.0.0.1:8020/
- http://127.0.0.1:8020/token/




# Локальная установка

Клонируйте репозиторий и перейдите в него в командной строке:

```sh
git clone https://gitlab.com/test_my3/salary_updater.git && cd test_task_shift_django
```


файл .env прилогается в проекте, однако рекомендую заменить данные на свои:

```sh
#.env
DB_HOST=<db>
DB_PORT=<5432>
DB_USER=<postgres>
DB_PASSWORD=<postgres>
DB_NAME=<postgres>
SECRET_KEY=<6few3nci_q_o@l1dlbk81%wcxe!*6r29yu629&d97!hiqat9fa>
SALT=<"salary_manager">
```
Перейдите в директорию с файлом _docker-compose.yml_ и выполнить команду docker compose up -d:

```sh
docker compose up -d
```


После успешного запуска контейнеров выполните миграции в проекте:

```sh
docker  exec backend python manage.py makemigrations db
```

```sh
docker exec backend python manage.py migrate
```



Наполните БД заготовленными данными:

- Пользователи и профайлы для них:

```sh
docker exec backend python manage.py loaddata db.json
```


Для остановки контейнеров и удаления всех зависимостей воспользуйтесь командой:

```sh
docker-compose down -v
```

# Примеры запросов

**Post**: http://127.0.0.1:8020/token/

Тело запроса:
```json
{
  "username": "Denis",
  "password": "I_love_python"
}
```
Пример ответа:
```json
{
    "Token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IkFsZXhhbmRlciJ9.2BpnGyH2yC8WqEeaWGl1ZRTFgY-x8eGwG5Hb9uNSMK0"
}
```

**POST**: http://127.0.0.1:8000/
Тело запроса:
```json
{
  "username": "Denis",
  "password": "I_love_python"
}
```
 
Пример ответа:
```json
{
    "salary: ": 1828.0,
    "update date: ": "2024-05-06"
}
```