import sys

try:
    from django.db import models
except Exception:
    print('Exception: Django Not Found, please install it with "pip install django".')
    sys.exit()


# Sample User model
class User(models.Model):
    username = models.CharField(max_length=50, default="Dan")
    password = models.CharField(max_length=250)

    def __str__(self):
        return self.username


class UserProfile(models.Model):
    salary = models.DecimalField(max_digits=20, decimal_places=2)
    update_date = models.DateField()
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="user_profile")

    def data(self):
        return {"salary: ": self.salary, "update date: ": self.update_date}
