import hashlib
import django
import sys
import os
import jwt
from dotenv import load_dotenv
from random import randint
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from django.shortcuts import get_object_or_404
from fastapi import FastAPI, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm, OAuth2PasswordBearer
from datetime import datetime

sys.dont_write_bytecode = True
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"
django.setup()

from db.models import User, UserProfile

app = FastAPI()
load_dotenv()
SECRET_KEY = os.getenv("SECRET_KEY")
SALT = os.getenv("SALT")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


def get_hashed_password(password):
    return hashlib.sha256(password.encode("utf-8") + SALT.encode("utf-8")).hexdigest()


def authenticate_user(username: str, password: str) -> User:
    try:
        user = get_object_or_404(
            User,
            username=username,
            password=get_hashed_password(password)
        )
        return user
    except ObjectDoesNotExist:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="User with this credentials were not found."
        )


@app.post('/token')
async def get_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user = authenticate_user(
        form_data.username,
        form_data.password
    )
    if user is None:
        raise HTTPException(
            detail="Неверные имя пользователя или пароль",
            status_code=401
        )
    access_token = jwt.encode(
        {"username": user.username},
        SECRET_KEY,
        algorithm="HS256"
    )
    return {"Token": access_token}


@app.post('/')
async def test(token: str = Depends(oauth2_scheme)):
    try:
        data = jwt.decode(token, SECRET_KEY, algorithms=["HS256"])
        try:
            user = get_object_or_404(User, username=data["username"])
            return user.user_profile.data()
        except Http404:
            return {"message": "User not found"}

    except jwt.ExpiredSignatureError:
        raise HTTPException(status_code=401, detail="Токен истек")


@app.post('/create')
async def create_user(form_data: OAuth2PasswordRequestForm = Depends()):
    try:
        return authenticate_user(
            username=form_data.username,
            password=form_data.password
        )
    except Http404:
        user = User(
            username=form_data.username,
            password=get_hashed_password(form_data.password)
        )
        user_profile = UserProfile(
            user=user,
            salary=randint(1000, 3000),
            update_date=datetime.now()
        )
        user.save()
        user_profile.save()
        return user
